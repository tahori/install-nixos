# Start with
# 1
# nix-shell https://github.com/sgillespie/nixos-yubikey-luks/archive/master.tar.gz
# 2
# curl https://gitlab.com/tahori/install-nixos/-/raw/main/install-with-luks-yubikey.bash
set -Eeuo pipefail

volume="${1}"

step () {
  text="${1}"
  printf '\n\e[92m\e[1m[[ %s ]]\e[21m\e[0m\n\n' "${text}"
}

substep () {
  text="${1}"
  printf '\n\e[96m\e[1m[[ %s ]]\e[21m\e[0m\n\n' "${text}"
}

rbtohex() {
  ( od -An -vtx1 | tr -d ' \n' )
}

hextorb() {
  ( tr '[:lower:]' '[:upper:]' | sed -e 's/\([0-9A-F]\{2\}\)/\\\\\\x\1/gI'| xargs printf )
}

step "Starting installing nixos using volume ${volume}"

# vd='/dev/sda'

substep 'Preparing physical volumes UEFI (GPT)'
parted "${volume}" -- mklabel gpt
parted "${volume}" -- mkpart nixos 512MiB 100%

substep 'Preparing /boot device size of 512MiB'
parted "${volume}" -- mkpart ESP fat32 1MiB 512MiB
parted "${volume}" print
boot_device_number=2
parted "${volume}" -- set "${boot_device_number}" boot on
mkfs.fat -F 32 -n boot "${volume}${boot_device_number}"

step 'Preparing yubikey with luks'
set -x
# # nix-shell https://github.com/sgillespie/nixos-yubikey-luks/archive/master.tar.gz
# # This is already done for the yubikey, but...
# # yubikey free slot is programmed like:
# # ykpersonalize -2 -ochal-resp -ochal-hmac
SALT_LENGTH=16
# PRE_SALT="$(dd if=/dev/urandom bs=1 count=${SALT_LENGTH} 2>/dev/null)"
# echo "${PRE_SALT}"
# SALT="$(echo ${PRE_SALT} | rbtohex)"
# echo "${SALT}"
SALT="$(dd if=/dev/urandom bs=1 count=${SALT_LENGTH} 2>/dev/null | rbtohex)"
read -s USER_PASSPHRASE
CHALLENGE="$(echo -n ${SALT} | openssl dgst -binary -sha512 | rbtohex)"
cryptsetup luksDump "${volume}1"
RESPONSE=$(ykchalresp -2 -x ${CHALLENGE} 2>/dev/null)
KEY_LENGTH=512
ITERATIONS=1000000
LUKS_KEY="$(echo -n ${USER_PASSPHRASE} | pbkdf2-sha512 $((${KEY_LENGTH} / 8)) ${ITERATIONS} ${RESPONSE} | rbtohex)"
CIPHER=aes-xts-plain64
HASH=sha512
echo -n "${LUKS_KEY}" | hextorb | cryptsetup luksFormat --use-urandom --cipher="${CIPHER}" --key-size="${KEY_LENGTH}" --hash="${HASH}" --key-file=- "${volume}1"
echo -n "${LUKS_KEY}" | hextorb | cryptsetup luksOpen "${volume}1" crypted --key-file=-

echo 'Preparing lvm on luks'
SWAP_SIZE='8G'
pvcreate /dev/mapper/crypted
vgcreate cryptedLVM /dev/mapper/crypted
lvcreate -L "${SWAP_SIZE}" cryptedLVM -n swap
lvcreate -l 100%FREE cryptedLVM -n root

echo 'Creating filesystem on lvm'
mkfs.ext4 -L nixos /dev/cryptedLVM/root
mkswap -L swap /dev/cryptedLVM/swap

echo 'Mounting the filesystems'
swapon /dev/cryptedLVM/swap
mount /dev/disk/by-label/nixos /mnt

mkdir -p /mnt/boot
mount /dev/disk/by-label/boot /mnt/boot
mkdir -p /mnt/boot/crypt-storage
echo -ne "${SALT}\n${ITERATIONS}" > /mnt/boot/crypt-storage/default

step 'Generating /root/.yubico/challenge-something'
nix-shell -p yubico-pam --run 'ykpamcfg -2 -v'

nixos-generate-config --root /mnt

step 'Prepare to install nixos'
ls -la /etc/nixos

# nixos-install

# nvme0n1p2
